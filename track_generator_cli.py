#!/usr/bin/env python3

import track_generator
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('output_file', help="csv output file. (default='track.csv')")

parser.add_argument('-s', '--seed', type=float, help="random number generator seed (default: random)")
parser.add_argument('-n', '--resolution', '--n-points', type=int, help="number of points sampled along the curve (default: Non-trivial)")
parser.add_argument('-r', '--min-corner-radius', type=float, help="minimum corner radius as measured to the center of the track (default: 3)")
parser.add_argument('-m', '--margin', type=float, help="minimum margin on either side of a track (default: 0)")

parser.add_argument('--starting-straight', type=float, help="the length of the starting straight (default: 6)")
# parser.add_argument('--starting-straight-downsample', type=int, help="reduces the accuracy of the metric used to pick a starting point (default: 2)")

cone_placement_group = parser.add_argument_group("Cone Placement")
cone_placement_group.add_argument('--min-cone-spacing', type=float, help="minimum distance between adjacent cones (default: 3π/16)")
cone_placement_group.add_argument('--max-cone-spacing', type=float, help="maximum distance between adjacent cones (default: 5)")
cone_placement_group.add_argument('-w', '--track-width', type=float, help="track width (default: 3)")
cone_placement_group.add_argument('-b', '--cone-bias', type=float, help="controls the cone spacing on the outside of a turn (default: 0.5)")
cone_placement_group.add_argument('--start-cone-separation', type=float, help="distance between the two pairs of big orange cones marking the starting line (default: 0.5)")

len_based_group = parser.add_argument_group("Length Based")
len_based_group.add_argument('-l', '--length', type=float, help="target track length. Overrides -f and -a flags")
len_based_group.add_argument('--rel-accuracy', type=float, help="maximum relative error in the track length (default: 0.005)")

param_based_group = parser.add_argument_group("Parameter Based")
param_based_group.add_argument('-f', '--max-freq', type=int, help="the highest frequency of the waves that make up the path (default: 7)",  dest="max_frequency")
param_based_group.add_argument('-a', '--amplitude', type=float, help="the amplitude of the waves that make up the path (default: 1/3)")
# param_based_group.add_argument('-c', '--check-self-intersects', type=float, help="guarantees that the generated track has at least margin margin (default: True)")

parser.set_defaults(output_file = 'track.csv')

args = parser.parse_args()

# generate track
gen = track_generator.TrackGenerator({
    key : args.__dict__[key] for key in args.__dict__
        if args.__dict__[key] != None
})
start_cones, left_cones, right_cones = gen()
track_generator.write_to_csv(args.output_file, start_cones, left_cones, right_cones)
