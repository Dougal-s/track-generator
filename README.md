# Track Generator

## Index:

* [Usage](#usage)
  * [API](#api)
  * [track_generator.py](#track_generatorpy)
* [Path Generation](#path-generation)
  * [Self intersection](#self-intersection)
* [Starting Line Selection](#starting-line-selection)
* [Cone Placement](#cone-placement)

## Usage

### API

```python
from track_generator import TrackGenerator

generator = TrackGenerator({
    # dictionary containing parameter values
})

# generate 20 different tracks
for _ in range(20):
	# left and right cone positions in the complex plain as a numpy array
	left_cones, right_cones = generator()
	# Do something...
```

The generator has two parameter sets. The first allows for targeting a track length within a specified accuracy, and is the one you would typically use. The second accepts parameters that directly map to variables in the equation that define the shape of the track. Since these variables affect the track in non-trivial ways, this parameter set is less useful for track generation, and more useful for testing and exploring the properties of the equation.

#### Length Based:

| Parameter      | Description                            | Type  | Default   |
| -------------- | -------------------------------------- | ----- | --------- |
| 'length'       | Target track length                    | float | Undefined |
| 'rel_accuracy' | Maximum relative error in track length | float | 0.005     |

#### Parameter Based:

| Parameter                 | Description                                                  | Type  | Default |
| ------------------------- | ------------------------------------------------------------ | ----- | ------- |
| 'max_frequency'           | The highest frequency of the waves that make up the path     | int   | 7       |
| 'amplitude'               | The amplitude of the waves that make up the path             | float | 1/3     |
| 'check_self_intersection' | Guarantees that the generated track has at least margin `margin` | bool  | True    |

#### Both:

| Parameter                      | Description                                                  | Type  | Default     |
| ------------------------------ | ------------------------------------------------------------ | ----- | ----------- |
| 'seed'                         | Random number generator seed                                 | float | Random      |
| 'resolution'                   | Number of points sampled along the curve                     | int   | Non-trivial |
| 'min_corner_radius'            | The minimum corner radius as measured to the center of the track  | float | 3           |
| 'starting_straight_length'     | Length of the starting straight (the starting straight is the stretch with the least curvature and may not be exactly straight) | float | 6           |
| 'starting_straight_downsample' | Reduces the accuracy of the metric used to pick a starting point  | int   | 2           |
| 'starting_cone_spacing'        | distance between the two pairs of cones marking the starting line | float | 0.5 |
| 'min_cone_spacing'             | Minimum distance between adjacent cones                      | float | 3π/16       |
| 'max_cone_spacing'             | Maximum distance between adjacent cones                      | float | 5           |
| 'track_width'                  | Track width                                                  | float | 3           |
| 'cone_spacing_bias'            | Controls the cone spacing on the outside of a turn           | float | 0.5         |
| 'margin'                       | Minimum margin on either side of a track                     | float | 0           |,

### track_generator.py

#### TrackGenerator
```python
class TrackGenerator:
    def __init__(self, config):
		
    def set(self, properties):

    def __call__(self):
```

##### __init__()
Initializes the track generator with the parameters defined by the dictionary `config`. See the [API](#api) section for the full list of parameters.

##### set()
Updates the parameters to the values in dictionary `properties`

##### __call__()
Generates the cones along a new racetrack.

**Returns**:
A tuple containing the cones on the left and right sides of the track

#### generate_path_w_params()
```python
def generate_path_w_params(rng, n_points, min_corner_radius, max_frequency, amplitude = 1/3):
```

Generates the spine of a random racetrack. Consider using [`generate_path_w_length`](#generate_path_w_length) instead as it has an easier to use parameter set.

**Returns**:
A tuple containing positions, normals, and corner radii along the path

| Arguments         | Description                                     | Type |
| ----------------- | ----------------------------------------------- | ---- |
| rng               | random number generator used to generate phases | any  |
| n_points          | sampling resolution. (i.e. the size of the returned position, normal, and corner radii lists) | int   |
| min_corner_radius | minimum corner radius. Since the track is scaled to achieve the minimum radius, increasing this will also increase the track length | float |
| max_frequency     | highest integer frequency component in the path. Increasing this increases the track length   | int   |
| amplitude         | amplitude of the waves the make up the path. Increasing this increases the track length       | float |

#### generate_path_w_length()
```python
def generate_path_w_length(rng, n_points, min_corner_radius, margin, target_track_length, rel_accuracy = 0.005):
```

Generates the spine of a random racetrack.

**Returns**:
A tuple containing positions, normals, and corner radii along the path

| Arguments           | Description                                     | Type |
| ------------------- | ----------------------------------------------- | ---- |
| rng                 | random number generator used to generate phases | any  |
| n_points            | sampling resolution. (i.e. the size of the returned position, normal, and corner radii arrays) | int |
| min_corner_radius   | the minimum corner radius                       | float |
| margin              | minimum margin on either side of the track      | float |
| target_track_length | the track length                                | float |
| rel_accuracy        | the maximum relative error in the track length  | float |


#### self_intersects()
```python
def self_intersects(points, slopes, margin):
```
returns true if the line defined by `points` and `slopes` with thickness 2*`margin`, self intersects


#### pick_starting_point()
```python
def pick_starting_point(positions, normals, corner_radii, starting_straight_length, downsample = 2):
```
Picks a suitable starting position, moves it to the beginning of the array, then translates and rotates the track such that the starting line faces 1,0 from 0,0

**Returns**:
A tuple containing the new position, normals, and corner_radii arrays

| Arguments                | Description                                                              | Type          |
| ------------------------ | ------------------------------------------------------------------------ | ------------- |
| positions                | points along the center of the track                                     | list[complex] |
| normals                  | normals along the centre of the track                                    | list[complex] |
| corner_radii             | corner radii along the centre of the track                               | list[float]   |
| starting_straight_length | the starting line is set to the end of the stretch of length starting_straight_length with the smallest average curvature | float |
| downsample               | reduces the number of points by factor downsample to improve performance | int           |


#### place_cones()
```python
def place_cones(
    positions, normals, corner_radii,
    min_cone_spacing, max_cone_spacing,
    track_width,
    cone_spacing_bias,
    start_offset,
    starting_cone_spacing
):
```
Generates starting, left and right cone locations from the track path

**Returns**:
A tuple containing the starting, left and right cone positions

| Arguments         | Description                                        | Type          |
| ----------------- | -------------------------------------------------- | ------------- |
| positions         | points along the center of the track               | list[complex] |
| normals           | normals along the centre of the track              | list[complex] |
| corner_radii      | corner radii along the centre of the track         | list[float]   |
| min_cone_spacing  | minimum distance between cones in metres           | float         |
| max_cone_spacing  | maximum distance between cones in metres           | float         |
| track_width       | track width in meters                              | float         |
| cone_spacing_bias | controls the cone spacing on the outside of a turn | float         |
| start_offset      | distance between the starting line and the car     | float         |
| starting_cone_spacing | distance between the two pairs of big orange cones marking the starting line | float         |

#### write_to_csv()
```python
def write_to_csv(file_path, start_cones, l_cones, r_cones):
```
Writes the cone positions to a csv file

| Arguments     | Description                                         | Type          |
| ------------- | --------------------------------------------------- | ------------- |
| file_path     | name of the csv output file                         | string        |
| start_cones   | locations of the cones marking the starting line    | list[complex] |
| l_cones       | locations of the cones along the left of the track  | list[complex] |
| r_cones       | locations of the cones along the right of the track | list[complex] |

## Path Generation

The track generator first generates the spine of the track by summing a series of waves wrapped around the unit circle with randomized phases. Mathematically, this is defined by

```math
P(z) = z \left[ 1 +
	A\sum_{k=2}^K\left(
		e^{-i\phi_k}\frac{z^k}{k+1}
		+ e^{i\phi_k}\frac{z^{-k}}{k-1}\right)
	\right]
```

with
```math
z=e^{i\theta} \text{ and } \theta \in [0, 2\pi)
```
where
```math
A = \text{amplitude},\\
K = \text{maximum frequency},\\
\phi_k = \text{randomized phase}
```


Visually, this is illustrated by Figures 1 and 2.

![component waves](component_waves.png)

Figure 1: component waves *k* = 2,...,5 with *A* = 0.5 and 𝜙 = 0.

![grid](grid.png)

Figure 2: *K* and *A* dependence of a track. From left to right: *K* = 2,...,9; from top to bottom: *A* = 0.2,...,0.5. The track length *L*, with minimum corner radius normalised to 3, is given above each track.

The distance along the track
```math
s = \int\left|\dot{P}\right|d\theta
```

where the tangent to the track
```math
\dot{P} \equiv \frac{dP}{d\theta} = izP'
```
The corner radius
```math
R = \frac{\left|\dot{P}\right|^3}{\Im(\dot{P}^*\ddot{P}) } = \frac{\left|P'\right|^3}{\Re(zP'^*P'') + \left|P'\right|^2}
```
which is positive for left turns and negative for right turns.


> ### Motivation for specific form of P(z)
>
> We chose the relative normalization of the positive and negative frequency terms, 1/(k+1) and 1/(k-1), to balance the curvature of the individual waves on the inside and outside turns:
> ```math
> \left.\frac{1}{R}\right|_\text{outside} = 1+2kA\\
> \left.\frac{1}{R}\right|_\text{inside} = 1-2kA
> ```
> where the 1 comes from the unit circle while the +-2kA come from the wave. Our form of P(z) also guarantees that it's derivative
> ```math
> P' = 1 + A\sum_{k=2}^K\left(e^{-i\phi_k}z^k - e^{i\phi_k}z^{-k}\right)
> ```
> is never zero for z on the unit circle. This prevents kinks and hence loops from forming. However, as can be seen in Figure 2, it does not prevent even intersection numbers.
>
> *Fun fact: the choice of coefficients also means that the length of the individual modes is independent of their frequency.*

Once the basic shape of the track is generated, we scale it in order to achieve a predefined minimum corner radius.

Unfortunately, we don't have a simple closed formula to generate parameters for a track of a specific length. Instead, we have to do this numerically. To start with, we set *A* to 0.4 and increase *K* until the track is longer than the desired track length. Then, we decrease *A* until we achieve the specified length. If the track ends up self intersecting, then we increase *K* and repeat.

###  Self intersection

In order to check for self intersection, we approximate the curve with a large number of line segments, and check whether any of these line segments intersect. To do this efficiently, we use a recursive partitioning algorithm that draws a line that splits the path into two roughly equally sized parts, which can each be handled independently.

## Starting Line Selection

Once we have the spine of the track, we need to pick a starting point. To do this, we apply a kernel smoother to the curvature of the track, and then find the global minimum of the resulting function. Mathematically, we are minimizing the function
```math
f(s)=\int_{s-2*r}^{s}\sin(\frac{\pi}{2}\times\frac{s-t}{r}) \frac{dt}{\left|R(t)\right|}
```

where
```math
r = 0.75\times\text{starting\_straight\_length}
```

## Cone Placement

When placing cones, each side is handled independently giving us exact control over the cone density
```math
\sigma = \frac{1}{\text{cone spacing}} = \frac{dn}{ds}
```
with cone number *n* for the left and right sides determined by
```math
dn_L=\sigma_\text{min}\,ds_L + \left(\sigma_\text{max}-\sigma_\text{min}\right) \left[
	\left(R_\text{min} - \frac w 2 \right) \frac{\left|d\theta\right|+d\theta}{2}
	+ \gamma \left(R_\text{min} + \frac w 2 \right) \frac{\left|d\theta\right|-d\theta}{2}
\right]\\
dn_R=\sigma_\text{min}\,ds_R + \left(\sigma_\text{max}-\sigma_\text{min}\right) \left[
	\left(R_\text{min} - \frac w 2 \right) \frac{\left|d\theta\right|-d\theta}{2}
	+ \gamma \left(R_\text{min} + \frac w 2 \right) \frac{\left|d\theta\right|+d\theta}{2}
\right]
```
where *w* is the track width and
```math
d\theta = \frac{ds}{R} = \frac{ds_L}{R-\frac w 2} = \frac{ds_R}{R+\frac w 2}
```
which is positive for left turns and negative for right turns. The first term sets the cone density to the minimum allowed on straights. The second term sets the cone density to the maximum allowed on the inside of the sharpest turn. The third and final term sets the cone density to
```math
\sigma_\text{min} + \gamma (\sigma_\text{max} - \sigma_\text{min})\\
```
on the outside of the sharpest turn. The cone density along the rest of the track interpolates linearly in the curvature.

> ### Special Cases
>
> Setting
> ```math
> \gamma = \gamma_1 = \frac{R_\text{min} - \frac w 2}{R_\text{min} + \frac w 2}
> ```
> results in a cone density
> ```math
> \sigma_L = \sigma_\text{min} + (\sigma_\text{max} - \sigma_\text{min})
> \frac{R_\text{min} - \frac w 2}{\left|R-\frac w 2 \right|}\\
> \sigma_R = \sigma_\text{min} + (\sigma_\text{max} - \sigma_\text{min})
> \frac{R_\text{min} - \frac w 2}{\left|R+\frac w 2 \right|}\\
> ```
> that only depends on the magnitude of the curvature along the cone line, i.e. the cone density depends only on the shape of the cone line itself and not on whether the cone line is on the inside or outside of a turn.
>
> Setting
> ```math
> \gamma = \gamma_0 = \frac{(\sigma_\text{max}-\sigma_\text{min})(R_\text{min} - \frac w 2) - w\sigma_\text{min}}{(\sigma_\text{max}-\sigma_\text{min})(R_\text{min} + \frac w 2)}
> ```
> results in cones on the left and right sides being placed opposite each other.

It is recommended to pick 𝛾 in the range
```math
\max(0, \gamma_0) \le \gamma \le \gamma_1
```
To facilitate this, we set
```math
\gamma = \left\{
\begin{array}{ll}
\max(0, \gamma_0) + \text{cone\_spacing\_bias} \times \left[\gamma_1-\max(0, \gamma_0)\right]
&
\text{if } \sigma_\text{min} < \sigma_\text{max}
\\
\text{cone\_spacing\_bias} \times \gamma_1
&
\text{if } \sigma_\text{min} = \sigma_\text{max}
\end{array}
\right.
```


The above formulation can be rearranged to calculate the cone density
```math
\sigma_L = \frac{dn_L}{ds_L}= \sigma_\text{min} + \frac{c_1}{R-\frac w 2} + \frac{c_2}{\left|R-\frac w 2 \right|}\\
\sigma_R = \frac{dn_R}{ds_R}= \sigma_\text{min} + \frac{c_1}{R+\frac w 2} + \frac{c_2}{\left|R+\frac w 2 \right|}
```
with
```math
c_1 = \frac 1 2 (\sigma_\text{max} - \sigma_\text{min})\left[\left(R_\text{min} - \frac w 2\right) - \gamma\left(R_\text{min} + \frac w 2\right)\right]\\
c_2 = \frac 1 2 (\sigma_\text{max} - \sigma_\text{min})\left[\left(R_\text{min} - \frac w 2\right) + \gamma\left(R_\text{min} + \frac w 2\right)\right]
```
The algorithm works by placing cones at integer values of the cone number. The integral is numerically computed at discrete points along the curve using a cumulative sum of the product of the cone density and the distance to the previous point. However,  this will typically result in a discontinuity between the first and last cones. To remedy this, we scale the cone spacing such that the integral of cone density over the length of the entire track is an integer. Since this spreads the discontinuity out over a large number of cones, the change in cone spacing should not be significant.
